﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Ein Logger, schreibt nur Events ins Log deren Auswirkung mindestens @minStage entspricht
/// Auswirkungen: DEBUG, INFO, WARNING, ERROR, CRITICAL
/// Ein Logfile ist in 3 Sektionen eingeteilt, wenn es die maximale Dateigröße überschreitet, wird die oberste Sektion gelöscht
/// Das Standardmaximum liegt bei 3MB
/// Die schlussendliche Größe kann leicht variieren
/// </summary>
public class Logger
{
    private String filepath;

    private int maxSections = 3;
    public uint MaxFileSize { get; set; } = 3000000;

    private long currentSectionSize = 0;
    private uint sectionCount = 1;

    public String SectionMarker { get; } = "########################################################################";

    public LogStage MinStage { get; set; } = LogStage.DEBUG;

    public Logger(String filepath, LogStage minStage = LogStage.DEBUG)
    {
        this.filepath = filepath;
        this.MinStage = minStage;

        String DirectoryPath = Path.GetDirectoryName(filepath);

        if (DirectoryPath != String.Empty)
        {

            if (Directory.Exists(DirectoryPath) == false)
            {
                throw new DirectoryNotFoundException(Path.GetDirectoryName(filepath));
            }

        }

        if (File.Exists(filepath))
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filepath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                long filesize = fs.Length;

                using (StreamReader sr = new StreamReader(fs))
                {
                    fs = null;
                    sectionCount = this.GetSectionCount(sr);
                    currentSectionSize = this.GetCurrentSectionSize(sr, sectionCount, filesize);
                }

            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }

        }

        else
        {
            using (FileStream fs = File.Create(filepath)) { }
        }
    }

    public void Log(String message, LogStage logStage)
    {
        WriteInto(message, logStage);
    }

    public void Debug(String message)
    {
        WriteInto(message, LogStage.DEBUG);
    }

    public void Info(String message)
    {
        WriteInto(message, LogStage.INFO);
    }

    public void Warning(String message)
    {
        WriteInto(message, LogStage.WARNING);
    }

    public void Error(String message)
    {
        WriteInto(message, LogStage.ERROR);
    }

    public void Critical(String message)
    {
        WriteInto(message, LogStage.CRITICAL);
    }

    private long GetCurrentSectionSize(StreamReader sr, uint sectionCount, long fileSize)
    {
        if (sectionCount == 1)
            return fileSize;

        long charCount = 0;


        while (!sr.EndOfStream)
        {
            String line = sr.ReadLine();

            if (line.Equals(this.SectionMarker))
            {
                charCount = 0;
            }
            else
            {
                charCount += System.Text.UTF8Encoding.UTF8.GetByteCount(line);
            }
        }

        return charCount;
    }

    private void StartNewSection(StreamWriter sw)
    {
        sw.WriteLine(SectionMarker);
        sectionCount++;
        currentSectionSize = 0;
    }

    private void DeleteOldestSection(String filepath)
    {

        List<String> lines = new List<string>();

        using (FileStream fs = new FileStream(filepath, FileMode.Open))
        {

            String currentLine = "";

            using (StreamReader sr = new StreamReader(fs))
            {
                do
                {
                    currentLine = sr.ReadLine();
                } while (!currentLine.Equals(SectionMarker));

                while (!sr.EndOfStream)
                {
                    lines.Add(sr.ReadLine());
                }
            }

        }

        using (FileStream fs = new FileStream(filepath, FileMode.Create))
        {

            using (StreamWriter sw = new StreamWriter(fs))
            {
                foreach (String line in lines)
                {
                    sw.WriteLine(line);
                }
            }

        }

        sectionCount--;
    }

    private uint GetSectionCount(StreamReader sr)
    {
        uint counter = 1;

        while (!sr.EndOfStream)
        {
            String line = sr.ReadLine();

            if (line.Equals(SectionMarker))
            {
                counter++;
            }
        }

        return counter;
    }

    private void WriteInto(String message, LogStage stage)
    {
        if(this.MinStage > stage)
        {
            return;
        }

        String entry = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " [" + stage.ToString() + "]: " + message + "\n";

        FileStream fs = null;
        try
        {
            fs = new FileStream(this.filepath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);

            using (StreamWriter sw = new StreamWriter(fs))
            {
                fs = null;

                if (currentSectionSize + System.Text.UTF8Encoding.UTF8.GetByteCount(entry) > (long)((double)this.MaxFileSize / (double)this.maxSections))
                {
                    StartNewSection(sw);
                }

                sw.Write(entry);

                currentSectionSize += System.Text.UTF8Encoding.UTF8.GetByteCount(entry);
            }

        }

        finally
        {
            if (fs != null)
            {
                fs.Dispose();
            }
        }

        if (sectionCount > maxSections)
        {
            this.DeleteOldestSection(this.filepath);
        }
    }

}

public enum LogStage
{
    DEBUG, INFO, WARNING, ERROR, CRITICAL
}

