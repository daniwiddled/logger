﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tests
{
    [TestClass()]
    public class LoggerTests
    {
        [TestMethod()]
        public void Constructor_LogFileDoesntExist_LogFileCreated()
        {
            File.Delete("test.txt");

            Logger l = new Logger("test.txt", LogStage.DEBUG);

            Assert.AreEqual(true, System.IO.File.Exists("test.txt"));
        }

        [TestMethod()]
        public void Constructor_LogFileExist_LogFileCreated()
        {
            using (FileStream fs = File.Create("test3.txt")) { }

            Logger l = new Logger("test3.txt", LogStage.DEBUG);

            Assert.AreEqual(true, System.IO.File.Exists("test3.txt"));
        }

        [TestMethod()]
        public void Constructor_LogFileExist_LogFileDeleted()
        {
            using (FileStream fs = File.Create("test4.txt")) { }

            Logger l = new Logger("test4.txt", LogStage.DEBUG);

            File.Delete("test4.txt");

            Assert.AreEqual(false, System.IO.File.Exists("test4.txt"));
        }

        [TestMethod()]
        public void WriteInto_LogFileDoesntExist_FileContainsEntry()
        {
            File.Delete("test5.txt");

            Logger l = new Logger("test5.txt", LogStage.DEBUG);

            l.Debug("hallo");
            l.Critical("test");
            l.Error("test");
            l.Info("test");
            l.Warning("test");
            l.Log("test", LogStage.CRITICAL);

            List<String> Lines = new List<string>();

            Lines.AddRange(File.ReadAllLines("test5.txt"));

            Assert.AreEqual(6, Lines.Count);

            String firstLine = Lines[0];

            Assert.AreEqual(DateTime.Today.ToShortDateString(), ParseLogEntry(firstLine)[0]);
            Assert.AreEqual(DateTime.Now.Hour, DateTime.Parse(ParseLogEntry(firstLine)[1]).Hour);
            Assert.AreEqual("DEBUG", ParseLogEntry(firstLine)[2]);
            Assert.AreEqual("hallo", ParseLogEntry(firstLine)[3]);

            Assert.AreEqual("CRITICAL", ParseLogEntry(Lines[1])[2]);
            Assert.AreEqual("test", ParseLogEntry(Lines[1])[3]);

            Assert.AreEqual("ERROR", ParseLogEntry(Lines[2])[2]);
            Assert.AreEqual("test", ParseLogEntry(Lines[2])[3]);

            Assert.AreEqual("INFO", ParseLogEntry(Lines[3])[2]);
            Assert.AreEqual("test", ParseLogEntry(Lines[3])[3]);

            Assert.AreEqual("WARNING", ParseLogEntry(Lines[4])[2]);
            Assert.AreEqual("test", ParseLogEntry(Lines[4])[3]);

            Assert.AreEqual("CRITICAL", ParseLogEntry(Lines[5])[2]);
            Assert.AreEqual("test", ParseLogEntry(Lines[5])[3]);
        }

        [TestMethod()]
        public void SeperateFileInSections_FileReachesSectionLimit_SectionInserted()
        {
            File.Delete("test6.txt");

            Logger l = new Logger("test6.txt")
            {
                MaxFileSize = 150
            };
            l.Debug("TEST");
            l.Debug("Test");

            Assert.AreEqual("TEST", ParseLogEntry(File.ReadAllLines("test6.txt")[0])[3]);
            Assert.AreEqual(l.SectionMarker, File.ReadAllLines("test6.txt")[1]);
            Assert.AreEqual("Test", ParseLogEntry(File.ReadAllLines("test6.txt")[2])[3]);
        }

        [TestMethod()]
        public void DeleteOldestSection_FileReachesSizeLimit_OldestSectionDeleted()
        {
            File.Delete("test7.txt");

            Logger l = new Logger("test7.txt")
            {
                MaxFileSize = 10000
            };
            for (int x = 0; x < 10000; x++)
            {
                l.Debug("Haaaalloo");
            }

            Assert.IsTrue(10100 >= new FileInfo("test7.txt").Length);
        }

        [TestMethod()]
        public void GetCurrentSectionSize_FileAlreadyExists_GetCorrectSectionSize()
        {
            File.Delete("test8.txt");
            Logger l = new Logger("test8.txt")
            {
                MaxFileSize = 10000
            };
            for (int x = 0; x < 9900; x++)
            {
                l.Debug("HAAAALLOOOOO");
            }

            l = null;

            Logger l2 = new Logger("test8.txt")
            {
                MaxFileSize = 10000
            };
            for (int x = 0; x < 10000; x++)
            {
                l2.Debug("HAAAALLOOOOO");
            }

            Assert.IsTrue(10100 >= new FileInfo("test8.txt").Length);
        }

        [TestMethod()]
        public void Constructor_DirectoryNotFound_ExceptionThrown()
        {
            try
            {
                Logger l = new Logger(@"a\b\c\d\e\f\g\h.txt");
                Assert.Fail();
            }
            catch (System.IO.DirectoryNotFoundException)
            {

            }

        }

        [TestMethod()]
        public void MinStages_MinStageWarning_LowerStagesNotInLogFile()
        {
            File.Delete("test9.txt");
            Logger l = new Logger("test9.txt", LogStage.WARNING);

            l.Debug("test");
            l.Info("test");
            l.Warning("test");
            l.Error("test");
            l.Critical("test");

            l = null;

            String[] lines = File.ReadAllLines("test9.txt");

            List<String> logTypes = new List<string>();

            foreach(String line in lines)
            {
                logTypes.Add(ParseLogEntry(line)[2]);
            }

            Assert.AreEqual(false, logTypes.Contains("INFO"));
            Assert.AreEqual(false, logTypes.Contains("DEBUG"));
            Assert.AreEqual(true, logTypes.Contains("WARNING"));
            Assert.AreEqual(true, logTypes.Contains("ERROR"));
            Assert.AreEqual(true, logTypes.Contains("CRITICAL"));
        }

        [TestMethod()]
        public void MinStages_MinStageInfo_LowerStagesNotInLogFile()
        {
            File.Delete("test10.txt");
            Logger l = new Logger("test10.txt", LogStage.INFO);

            l.Debug("test");
            l.Info("test");
            l.Warning("test");
            l.Error("test");
            l.Critical("test");

            l = null;

            String[] lines = File.ReadAllLines("test10.txt");

            List<String> logTypes = new List<string>();

            foreach (String line in lines)
            {
                logTypes.Add(ParseLogEntry(line)[2]);
            }

            Assert.AreEqual(true, logTypes.Contains("INFO"));
            Assert.AreEqual(false, logTypes.Contains("DEBUG"));
            Assert.AreEqual(true, logTypes.Contains("WARNING"));
            Assert.AreEqual(true, logTypes.Contains("ERROR"));
            Assert.AreEqual(true, logTypes.Contains("CRITICAL"));
        }

        [TestMethod()]
        public void MinStages_MinStageDebug_LowerStagesNotInLogFile()
        {
            File.Delete("test11.txt");
            Logger l = new Logger("test11.txt", LogStage.DEBUG);

            l.Debug("test");
            l.Info("test");
            l.Warning("test");
            l.Error("test");
            l.Critical("test");

            l = null;

            String[] lines = File.ReadAllLines("test11.txt");

            List<String> logTypes = new List<string>();

            foreach (String line in lines)
            {
                logTypes.Add(ParseLogEntry(line)[2]);
            }

            Assert.AreEqual(true, logTypes.Contains("INFO"));
            Assert.AreEqual(true, logTypes.Contains("DEBUG"));
            Assert.AreEqual(true, logTypes.Contains("WARNING"));
            Assert.AreEqual(true, logTypes.Contains("ERROR"));
            Assert.AreEqual(true, logTypes.Contains("CRITICAL"));
        }

        [TestMethod()]
        public void MinStages_MinStageError_LowerStagesNotInLogFile()
        {
            File.Delete("test12.txt");
            Logger l = new Logger("test12.txt", LogStage.ERROR);

            l.Debug("test");
            l.Info("test");
            l.Warning("test");
            l.Error("test");
            l.Critical("test");

            l = null;

            String[] lines = File.ReadAllLines("test12.txt");

            List<String> logTypes = new List<string>();

            foreach (String line in lines)
            {
                logTypes.Add(ParseLogEntry(line)[2]);
            }

            Assert.AreEqual(false, logTypes.Contains("INFO"));
            Assert.AreEqual(false, logTypes.Contains("DEBUG"));
            Assert.AreEqual(false, logTypes.Contains("WARNING"));
            Assert.AreEqual(true, logTypes.Contains("ERROR"));
            Assert.AreEqual(true, logTypes.Contains("CRITICAL"));
        }

        [TestMethod()]
        public void MinStages_MinStageCritical_LowerStagesNotInLogFile()
        {
            File.Delete("test13.txt");
            Logger l = new Logger("test13.txt", LogStage.CRITICAL);

            l.Debug("test");
            l.Info("test");
            l.Warning("test");
            l.Error("test");
            l.Critical("test");

            l = null;

            String[] lines = File.ReadAllLines("test13.txt");

            List<String> logTypes = new List<string>();

            foreach (String line in lines)
            {
                logTypes.Add(ParseLogEntry(line)[2]);
            }

            Assert.AreEqual(false, logTypes.Contains("INFO"));
            Assert.AreEqual(false, logTypes.Contains("DEBUG"));
            Assert.AreEqual(false, logTypes.Contains("WARNING"));
            Assert.AreEqual(false, logTypes.Contains("ERROR"));
            Assert.AreEqual(true, logTypes.Contains("CRITICAL"));
        }

        public List<String> ParseLogEntry(String logEntry)
        {
            List<String> items = new List<String>();

            String date = logEntry.Substring(0, logEntry.IndexOf(' '));
            String time = logEntry.Substring(date.Length + 1, logEntry.IndexOf(' ', date.Length + 1) - date.Length - 1);
            String mode = logEntry.Substring(logEntry.IndexOf('[') + 1, logEntry.IndexOf(']') - (date.Length + 1 + time.Length + 1) - 1);
            String message = logEntry.Substring(logEntry.IndexOf(' ', date.Length + time.Length + mode.Length) + 1);

            items.Add(date);
            items.Add(time);
            items.Add(mode);
            items.Add(message);

            return items;
        }

    }
}